# GitLab Pipeline Template for Terraform Projects

This is a comprehensive template designed to streamline the configuration of GitLab pipelines for your Terraform projects, providing an efficient and standardized approach to managing your infrastructure-as-code workflows.

Incorporate the template into your project by adding the following snippet to your `.gitlab-ci.yml` file:

```yml
include:
  - project: adarlan/gitlab-pipeline-template-for-terraform
    file: template.gitlab-ci.yml
    ref: v1
```

This template comes equipped with a series of pipeline stages to manage your infrastructure stacks effectively. The stages include:

- **Test**: Runs jobs to validate Terraform configurations and check for proper formatting.
- **Plan**: Generates a Terraform plan to preview changes before applying them.
- **Apply**: Applies the changes to your infrastructure (Manual or Automatic).
- **Rollback** (Optional): Automates the rollback process to maintain a consistent infrastructure state.
- **Destroy** (Optional): Provides the ability to gracefully destroy the infrastructure when no longer needed.

## Getting Started

To get started with this pipeline template, follow these steps:

Create a new GitLab repository.

Add the necessary `.tf` files containing your Terraform configuration.

```tf
module "my_cloud_stack" {
  source      = "my-cloud-stack-module"
  stack_name  = "my_stack"
  description = "My cloud infrastructure stack"
  vpc_id      = var.vpc_id
  subnet_ids  = var.subnet_ids
}
variable "vpc_id" { type = string }
variable "subnet_ids" { type = list(string) }
```

Add the `.gitlab-ci.yml` file with the `include` section specifying that your project will use this pipeline template.
Use the `variables` section to set the non-sensitive variables of your Terraform configuration. The `TF_VAR_` prefix is used by Terraform to automatically set these variables.

```yml
include:
  - project: adarlan/gitlab-pipeline-template-for-terraform
    file: template.gitlab-ci.yml
    ref: v1

variables:
  TF_VAR_vpc_id: 'vpc-12345678'
  TF_VAR_subnet_ids: '["subnet-abcdefgh", "subnet-ijklmnop"]'
```

After you commit and push the changes to the `main` branch, the pipeline will be started automatically.

The normal pipeline has three stages: Test, Plan, and Apply.

![](img/plan-running.png)

To further configure the pipeline stages according to your project specifics, you can add the following variables to your `.gitlab-ci.yml` file.

```yml
variables:
  ALLOW_CHECK_FORMAT_FAILURE: 'true'
  AUTO_APPLY: 'true'
  ENABLE_ROLLBACK: 'true'
```

All these variables are optional and are already configured with a default value. Add only the variables you need to change the default pipeline workflow.

## Sensitive Variables

Your Terraform configuration probably requires sensitive information, like the variables in the example below.

```tf
module "my_cloud_stack" {
  # ...
  database_password = var.database_password
  private_key       = var.private_key
}

variable "database_password" {
  type      = string
  sensitive = true
}

variable "private_key" {
  type      = string
  sensitive = true
}
```

To configure these sensitive variables, create protected variables with the `TF_VAR_` prefix in your repository settings.

GitLab repository >> Settings >> CI/CD >> Variables >> Add variable

![](img/sensitive-variables.png)

## Backend and Providers

For Terraform backend configuration, add a protected file variable named `TF_BACKEND` in your repository settings.

Example:

![](img/variable-tf-backend.png)

For providers configuration, add a protected file variables with the `TF_PROVIDER_` prefix in your repository settings.

Example:

![](img/variable-tf-provider.png)

## Test

The Test stage runs two jobs in parallel: CheckFormat and Validate. The CheckFormat job checks the formatting of your `.tf` files, and the Validate job validates the Terraform configuration.

![](img/test-running.png)

If your `.tf` files are not well formatted, the CheckFormat job will fail.

![](img/check-format-failed.png)

However, you can allow the pipeline to proceed even if CheckFormat fails by enabling the following variable in your `.gitlab-ci.yml`:

```yml
variables:
  ALLOW_CHECK_FORMAT_FAILURE: 'true'
```

In that case it will show as warning instead of failed, and the pipeline will continue.

![](img/check-format-warning.png)

The Validate job, on the other hand, cannot be allowed to fail. If there is a configuration mistake, this job will fail, and the pipeline won't proceed.

![](img/validate-failed.png)

## Plan

After the Test stage, the Plan stage is executed, allowing you to preview the changes before applying them.

![](img/plan-running.png)

Even after passing the tests, it's still possible for the Plan stage to fail, for example, if you create a required variable but do not define its value.

In this case, the pipeline will fail, and the changes will not be applied.

![](img/plan-failed.png)

## Apply

The Apply stage is manual, by default, allowing you to review the plan and decide whether you want to apply the changes or not.

![](img/apply-manual.png)

You can configure it to be automatic by setting the following variable in your `.gitlab-ci.yml`:

```yml
variables:
  AUTO_APPLY: 'true'
```

When `AUTO_APPLY` is enabled, the Apply job will start automatically after the Plan job.

![](img/apply-running.png)

Even after passing the tests and having a successful plan, it's still possible for the Apply stage to fail.

![](img/apply-failed.png)

In this case, your changes may be partially applied, with some resources created and others not. This could leave your infrastructure stack in an inconsistent state.

## Rollback

To be prepared for situations when the Apply stage fails, you can enable the Rollback stage by setting the following variable in your `.gitlab-ci.yml`:

```yml
variables:
  ENABLE_ROLLBACK: 'true'
```

When the Rollback stage is enabled, and a successful apply happens, the TagStable job runs, marking the successful commit with the `stable` version tag.

![](img/tag-stable-running.png)

![](img/graph-stable-tag.png)

If, in later changes, the Apply stage fails, the Rollback job will be automatically executed. It will checkout the `stable` version and apply it, making your infrastructure stack consistent again.

![](img/rollback-running.png)

The rollback is disabled by default because it requires permissions to push the `stable` tag. You can configure these permissions by setting the following protected variables in your repository settings:

- `GITLAB_USER_NAME`: Your GitLab username.
- `GITLAB_USER_EMAIL`: Your GitLab email address.
- `GITLAB_USER_WRITE_REPOSITORY_TOKEN`: A GitLab personal access token with write access to the repository.

## Destroy

When the infrastructure stack is no longer needed, you can configure it to be destroyed by setting the following variable in your `.gitlab-ci.yml` file:

```yml
variables:
  DESTROY: 'true'
```

The pipeline will then run with only two stages, PlanDestroy and ApplyDestroy, allowing you to review the resources that will be destroyed before applying it.

![](img/destroy-manual.png)

If you configured `AUTO_APPLY=true`, the ApplyDestroy job will be automatically executed after PlanDestroy.

![](img/destroy-running.png)

If your infrastructure stack is used temporarily for testing, for example, and you want to ensure that the created resources are destroyed after use, you can create a pipeline schedule configured to run periodically, enabling `DESTROY=true` and `AUTO_APPLY=true`.

![](img/destroy-everyday.png)

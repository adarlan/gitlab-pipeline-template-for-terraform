# Tests

## Success

```tf
provider "null" {}
resource "null_resource" "success" {
  triggers = {
    foo = "Foo"
    bar = "Bar"
  }
  provisioner "local-exec" {
    command = "exit 0"
  }
}
```

## CheckFormat fail

```tf
provider "null" {}
resource "null_resource" "check_format_fail" {
  triggers  = {
    foo            = "Foo"
      foo      =  "Bar"
   }
   provisioner "local-exec" {
       command =   "exit 0"
  }
}
```

## Validate fail

```tf
provider "null" {}
resource "null_resource" "validate_fail" {
  triggers = {
    foo = "Foo"
  }
  bar = "Bar"
  provisioner "local-exec" {
    command = "exit 0"
  }
}
```

## Plan fail

```tf
provider "null" {}
resource "null_resource" "plan_fail" {
  triggers = {
    foo = "Foo"
    bar = var.bar
  }
  provisioner "local-exec" {
    command = "exit 0"
  }
}
variable "bar" {
  type = string
}
```

## Apply fail

```tf
provider "null" {}
resource "null_resource" "apply_fail" {
  triggers = {
    foo = "Foo"
    bar = "Bar"
  }
  provisioner "local-exec" {
    command = "exit 1"
  }
}
```

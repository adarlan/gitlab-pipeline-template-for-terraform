provider "null" {}
resource "null_resource" "success" {
  triggers = {
    foo = "Foo"
    bar = "Bar"
  }
  provisioner "local-exec" {
    command = "exit 0"
  }
}